﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TowerManager : MonoBehaviour
{
    public GameObject towerPrefab;
    public Text towersCountText;

    private List<Tower> _towers = new List<Tower>();

    private void Start()
    {
        var tower = InstantiateNewTower(Vector2.zero);
        tower.GetComponent<Tower>().SetTowerActive();
    }

    public GameObject InstantiateNewTower(Vector2 location)
    {
        if (_towers.Count == 100)
        {
            SetAllTowersAsActive();
            return null;
        }
        
        var tower = Instantiate(towerPrefab, location, Quaternion.identity);
        var towerComponent = tower.GetComponent<Tower>();
        towerComponent.OnTowerDestroyed += UpdateTowersCount;
        _towers.Add(towerComponent);
        
        UpdateTowersCountText();
        
        return tower;
    }

    private void UpdateTowersCountText()
    {
        towersCountText.text = "Towers: " + _towers.Count;
    }

    private void UpdateTowersCount(Tower tower)
    {
        _towers.Remove(tower);
        Destroy(tower.gameObject);
        UpdateTowersCountText();
    }

    private void SetAllTowersAsActive()
    {
        _towers.ForEach(tower => { tower.SetTowerActive(); });
    }
}

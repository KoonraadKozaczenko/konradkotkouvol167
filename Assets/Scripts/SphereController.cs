﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class SphereController : MonoBehaviour
{
	private Rigidbody2D _rigidbody;
	private Vector2 _mousePositionInWorldSpace;
	private bool _isActive;

	private void Start()
	{
		_rigidbody = GetComponent<Rigidbody2D>();
	}

	private void OnMouseDown()
	{
		_isActive = true;
	}

	private void OnMouseUp()
	{
		_isActive = false;
	}

	private void OnMouseDrag()
	{
		_mousePositionInWorldSpace = Camera.main.ScreenToWorldPoint(Input.mousePosition);
	}

	private void FixedUpdate()
	{
		if (!_isActive) return;

		_rigidbody.MovePosition(_rigidbody.position + _mousePositionInWorldSpace * Time.fixedDeltaTime);
	}
}

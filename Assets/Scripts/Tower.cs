﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(SpriteRenderer))]
public class Tower : MonoBehaviour
{
	public int shootTimes = 12;
	public GameObject bulletPrefab;
	public Vector2 bulletSpawnOffset;
	public Color activeColor;
	public Color inactiveColor;

	public event Action<Tower> OnTowerDestroyed;
	
	private Quaternion _targetAngle;
	private SpriteRenderer _spriteRenderer;
	private bool _isActive;

	private void Awake()
	{
		_spriteRenderer = GetComponent<SpriteRenderer>();
		_spriteRenderer.color = inactiveColor;
	}
	
	private void Start()
	{
		StartCoroutine(SetTowerActiveCoroutine());
	}

	private IEnumerator Turn()
	{
		for (var i = 1; i <= shootTimes; i++)
		{
			_targetAngle = Quaternion.Euler(transform.eulerAngles + Vector3.forward * Random.Range(15, 45));
			transform.rotation = _targetAngle;

			var offset = transform.rotation * bulletSpawnOffset;
			
			Instantiate(bulletPrefab, (Vector2)transform.position + (Vector2) offset, transform.rotation);
			
			yield return new WaitForSeconds(.5f);
		}

		_isActive = false;
		_spriteRenderer.color = inactiveColor;
	}

	private IEnumerator SetTowerActiveCoroutine()
	{
		if (_isActive) yield break;
		
		yield return new WaitForSeconds(6);
		SetTowerActive();
	}

	public void SetTowerActive()
	{
		if (_isActive) return;
		
		_isActive = true;
		_spriteRenderer.color = activeColor;
		StartCoroutine(Turn());
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (!other.gameObject.CompareTag("bullet")) return;

		if (OnTowerDestroyed != null)
		{
			OnTowerDestroyed.Invoke(this);
		}
		
		Destroy(other.gameObject);
	}
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(Rigidbody2D))]
public class Bullet : MonoBehaviour
{
	public float speed = 4.0f;

	private Vector2 _maxDistance;
	private TowerManager _towerManager;
	
	private void Start ()
	{
		_towerManager = FindObjectOfType<TowerManager>();
		
		var rigidbody = GetComponent<Rigidbody2D>();
		rigidbody.velocity = transform.up * speed;

		var oneUnitTime = 1 / speed;
		var unitDistance = Random.Range(1, 4);

		StartCoroutine(DestroyObject(oneUnitTime * unitDistance));
	}

	private IEnumerator DestroyObject(float time)
	{
		yield return new WaitForSeconds(time);
		gameObject.SetActive(false);
		_towerManager.InstantiateNewTower(transform.position);
		Destroy(gameObject);
	}
}
